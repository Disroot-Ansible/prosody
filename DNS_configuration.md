# DNS Configuration

It is important that you carefully read [https://prosody.im/doc/dns](https://prosody.im/doc/dns) to set up your server DNS.

Go on your DNS provider, and set your DNS records like this:

    _xmpp-client._tcp.server.org. 18000 SRV 0 5 5222 xmpp.server.org.
    _xmpp-server._tcp.server.org. 18000 SRV 0 5 5269 xmpp.server.org.
    _xmpps-client._tcp.server.org. 18000 SRV 0 5 5223 xmpp.server.org.
    server.org. 3600 A YOUR_IP_ADDRESS
    www.server.org. 3600 A YOUR_IP_ADDRESS
    muc.server.org. 3600 A YOUR_IP_ADDRESS
    pub.server.org. 3600 A YOUR_IP_ADDRESS
    proxy.server.org. 3600 A YOUR_IP_ADDRESS

Where:
- **YOUR_IP_ADDRESS** is your server IP address, something in this form 51.25.62.125
- **server.org** is your domain
- **xmpp.server.org** is your xmpp target domain
- **3600**, **18000** are the TTL (time-to-live)
- **0** is the priority
- **5** is the weight
- **5222**, **5269**, **5223** are the ports the services are running on, clients will typically connect to 5222, and servers to 5269.
- **muc** is the name you set for your muc server
- **pub** is the name you set for your pubsub server
- **proxy** is the name you set for your proxy server
