# Prosody - Ansible Role
This role deploys [prosody](https://prosody.im) XMPP server with all needed dependencies and prosody community modules.

You can deploy a test instance using `Vagrantfile` attached to the role. This role is released under MIT Licence. We do not give no warranty for this software. 

# Run in production
To run this role in production, do not forget to set your DNS as explained [here](./DNS_configuration.md) and to change all vars from files in `default/` to what you need.

The role's defaults should be already deploying fully functional and modern XMPP Server. It allows to add/remove any additional module (whether core or community). 

# Run test in Vagrant
To have it work using vagrant:
  - `cd prosody` where `prosody`is the name of this repo.
  - `vagrant up` to build up the Virtual Environment.
  - and `ansible-playbook -b Playbooks/prosody.yml` to deploy Prosody on the Virtual Environment.

# Setup turnserver for viop
If you want to use viop, you need to set `turncredentials` to `true` in `default/mod.yml`. This feature depends on a third party service that needs to be installed separately: coturn. To see how to set it, check [Prosody's documentation](https://prosody.im/doc/coturn)

# Add LDAP authentication and vhosts
To add LDAP authentication, edit `defaults/vhost.yml`, change `auth_method` from `internal_hashed` to `ldap` and change the LDAP vars in `prosody_ldap_config`.

# Virtual hosts
This role supports multiple vhosts.  In order to define them specify vhosts in `prosody_vhost` array. You can set number or settings. Make sure to provide certificates for each domain `prosody_certificate_path` eg:
```

 prosody_vhost:
   - name: 'example.org'
     enabled: 'true'
     carbon_defaults: 'true'
     auth_method: 'internal_hashed'
   - name: 'example.com'
     enabled: 'true'
     carbon_defaults: 'true'
     auth_method: 'ldap'
     prosody_ldap_config:
       - ldap_server: "{{ prosody_ldap_server }}"
         ldap_rootdn: "{{ prosody_ldap_rootdn }}"
         ldap_password: "{{ prosody_ldap_password }}"
         ldap_basedn: "{{ prosody_ldap_basedn }}"
         ldap_tls: "{{ prosody_ldap_tls }}"
         ldap_mode: "{{ prosody_ldap_mode }}"
```

# Modules 

